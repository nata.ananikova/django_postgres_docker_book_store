from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('books/', views.books_info, name='books'),
    path('books/<int:pk>/', views.show_book, name='book'),
    path('genres/', views.genres_info, name='genres'),
    path('genres/<int:pk>/', views.show_genre, name='genre'),
    path('authors/', views.authors_info, name='authors'),
    path('authors/<int:pk>/', views.show_author, name='author'),
    path('cities/', views.cities_info, name='cities'),
    path('cities/<int:pk>/', views.show_city, name='city'),
    path('add_client/', views.add_client, name='add_client'),
]

