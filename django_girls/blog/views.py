from django.db import connection
from django.shortcuts import render, get_object_or_404, redirect

from .forms import ClientForm
from .models import Genre, Book, Author, City, Client
from django.http import HttpResponseNotFound, Http404, HttpResponse

cursor = connection.cursor()


def index(request):
    index_menu = ['Genres', 'Books', 'Authors', 'Cities', 'Clients']
    return render(request, 'blog/index.html', context={'index_elems': {elements for elements in index_menu}})


data_books = {}


def books_info(request):
    cursor.execute('SELECT public.blog_book.book_id, public.blog_book.title, public.blog_book.price, '
                   'public.blog_book.amount, public.blog_author.name_author, '
                   'public.blog_genre.name_genre '
                   'FROM public.blog_book JOIN public.blog_author '
                   ' ON public.blog_book.author_id = public.blog_author.author_id '
                   ' JOIN public.blog_genre ON public.blog_book.genre_id = public.blog_genre.genre_id '
                   ' ORDER BY public.blog_book.title ')
    data_books['item'] = cursor.fetchall()
    return render(request, 'blog/books_info.html', data_books)


def show_book(request, pk):
    book_detail = get_object_or_404(Book, pk=pk)
    if pk < len(data_books):
        raise Http404()
    return render(request, 'blog/book_info.html', {'book_detail': book_detail})


# data_genres = {}
genres_queryset = Genre.objects.all()


def genres_info(request):
    # cursor.execute('SELECT * FROM public.blog_genre')
    # data_genres['item'] = cursor.fetchall()
    # return render(request, 'blog/genres_info.html', data_genres)
    return render(request, 'blog/genres_info.html', context={'genres_queryset': genres_queryset})


def show_genre(request, pk):
    genre_detail = get_object_or_404(Genre, pk=pk)
    if pk > len(genres_queryset):
        raise Http404()
    return render(request, 'blog/genre_info.html', {'genre_detail': genre_detail})


authors_queryset = Author.objects.all()


def authors_info(request):
    return render(request, 'blog/authors_info.html', context={'authors_queryset': authors_queryset})


def show_author(request, pk):
    author_detail = get_object_or_404(Author, pk=pk)
    if pk > len(authors_queryset):
        raise Http404()
    return render(request, 'blog/author_info.html', {'author_detail': author_detail})


cities_queryset = City.objects.all()


def cities_info(request):
    return render(request, 'blog/cities_info.html', context={'cities_queryset': cities_queryset})


def show_city(request, pk):
    city_detail = get_object_or_404(City, pk=pk)
    if pk > len(cities_queryset):
        raise Http404()
    return render(request, 'blog/city_info.html', {'city_detail': city_detail})


def add_client(request):
    if request.method == 'POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            try:
                Client.objects.create(**form.cleaned_data)
                return redirect('books/')
            except:
                form.add_error(None, 'Error')
    else:
        form = ClientForm()
    return render(request, 'blog/add_client.html', context={form: 'form'})


def page_not_found(request, exception):
    return render(request, 'blog/404.html')


