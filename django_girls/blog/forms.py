from django import forms
from .models import Genre, Author, Book, City, Client, Buy, BuyBook, Step, BuyStep


class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = ('name_genre', 'description_genre')


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ('name_author', 'description_author')


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ('title', 'price', 'amount', )


class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ('name_city', 'delivery')


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('name_client', 'email', )


class BuyForm(forms.ModelForm):
    class Meta:
        model = Buy
        fields = ('buy_description', )


class BuyBookForm(forms.ModelForm):
    class Meta:
        model = BuyBook
        fields = ('amount', )


class StepForm(forms.ModelForm):
    class Meta:
        model = Step
        fields = ('name_step', )


class BuyStepForm(forms.ModelForm):
    class Meta:
        model = BuyStep
        fields = ('data_step_beg', 'data_step_end', )