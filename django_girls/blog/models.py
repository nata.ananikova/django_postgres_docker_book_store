from audioop import reverse

from django.db import models


class Genre(models.Model):
    genre_id = models.CharField(max_length=64, primary_key=True)
    name_genre = models.CharField(max_length=64)
    description_genre = models.CharField(max_length=255)

    def __str__(self):
        return self.genre_id


class Author(models.Model):
    author_id = models.CharField(max_length=64, primary_key=True)
    name_author = models.CharField(max_length=128)
    description_author = models.CharField(max_length=255)

    def __str__(self):
        return self.author_id


class Book(models.Model):
    book_id = models.CharField(max_length=64, primary_key=True)
    title = models.CharField(max_length=128)
    author = models.ForeignKey('blog.Author', on_delete=models.CASCADE)
    genre = models.ForeignKey('blog.Genre', on_delete=models.CASCADE)
    price = models.CharField(max_length=128)
    amount = models.CharField(max_length=128)
    description_book = models.CharField(max_length=255)

    def __str__(self):
        return self.book_id


class City(models.Model):
    city_id = models.CharField(max_length=64, primary_key=True)
    name_city = models.CharField(max_length=128)
    delivery = models.CharField(max_length=128)

    def __str__(self):
        return self.city_id


class Client(models.Model):
    client_id = models.AutoField(primary_key=True)
    name_client = models.CharField(max_length=128)
    city = models.ForeignKey('blog.City', on_delete=models.PROTECT)
    email = models.EmailField(max_length=128)

    def __str__(self):
        return self.client_id


class Buy(models.Model):
    buy_id = models.CharField(max_length=64, primary_key=True)
    buy_description = models.TextField(blank=True, null=True)
    client = models.ForeignKey('blog.Client', on_delete=models.CASCADE)

    def __str__(self):
        return self.buy_id


class BuyBook(models.Model):
    buy_book_id = models.CharField(max_length=64, primary_key=True)
    buy = models.ForeignKey('blog.Buy', on_delete=models.CASCADE)
    book = models.ForeignKey('blog.Book', on_delete=models.CASCADE)
    amount = models.CharField(max_length=128)

    def __str__(self):
        return self.buy_book_id


class Step(models.Model):
    step_id = models.CharField(max_length=64, primary_key=True)
    name_step = models.CharField(max_length=128)

    def __str__(self):
        return self.step_id


class BuyStep(models.Model):
    buy_step_id = models.CharField(max_length=64, primary_key=True)
    buy = models.ForeignKey('blog.Buy', on_delete=models.CASCADE)
    step = models.ForeignKey('blog.Step', on_delete=models.CASCADE)
    data_step_beg = models.DateTimeField(blank=True, null=True)
    data_step_end = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.buy_step_id







